<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー管理画面</title>

<script type="text/javascript">
<!--

function check(){

	if(window.confirm('変更しますか？')){ //

		return true;

	}
	else{ // 「キャンセル」時の処理

		return false; // 送信を中止
	}

}

// -->
</script>
</head>
<body>
	<div class="main-contents">

		<a href="./">ホーム</a>
	 	<a href="signup">ユーザー新規登録</a>

	</div>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
	</c:if>

	<c:remove var="errorMessages" scope="session"/>

	<c:forEach items="${userList}" var="user">

        <div class="account">><c:out value="${user.account}" /></div>

        <div class="name"><c:out value="${user.name}" /></div>

        <div class="branchName"><c:out value="${user.branchName}" /></div>

        <div class="departmentName"><c:out value="${user.departmentName}" /> </div>

        <c:if test="${user.isStopped==1 }">
        	<div class="isStopped"><c:out value="停止中"/></div>
        </c:if>

        <c:if test="${user.isStopped==0 }">
        	<div class="isStopped"><c:out value="復活中"/></div>
        </c:if>

		<form action="setting" method="get">
        	<input type="hidden" name="userId" value="${ user.id }" />
        	<input type="submit" value="編集">
        </form>

		<form action="stop" method="post" onSubmit="return check()">

			<c:if test="${user.id != loginUser.id}">
			<c:if test="${user.isStopped==1}">
				<input type="hidden" name="userId" value="${user.id}">
				<input type="hidden" name="isStopped" value="0">
				<input type="submit" value="復活" >
			</c:if>
			</c:if>

			<c:if test="${user.id != loginUser.id}">
			<c:if test="${user.isStopped ==0}">
				<input type="hidden" name="userId" value="${user.id}">
				<input type="hidden" name="isStopped" value="1">
				<input type="submit" value="停止" >
			</c:if>
			</c:if>

		</form>

		<div class="copyright">Copyright(c)usui_arisa</div>

	</c:forEach>
</body>
</html>