<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>

<script type="text/javascript">
<!--

function check(){

	if(window.confirm('削除しますか？')){ //
		return true;
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}
}

// -->
</script>

</head>
<body>
	<div class="main-contents">

		<c:if test="${ empty loginUser }">
			<a href="signup">新規登録</a>
			<a href="login">ログイン</a>
		</c:if>

		<c:if test="${ not empty loginUser }">
			<a href="message">新規投稿</a>

			<c:if test = "${loginUser.branchId == 1 and loginUser.departmentId ==1}">
			<a href="management">ユーザー管理</a>
			</c:if>>

			<a href="logout">ログアウト</a>
		</c:if>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
        </c:if>

		<c:remove var="errorMessages" scope="session"/>

		<form action="/" method="get">
			<label>
			日付<input type="date" name="start" value="${start}" >～<input type="date" name="end" value="${end}">
			</label>
			<br>
			<label>
			カテゴリ<input name="category" id="name" value="${category}"/> <input type="submit" value="絞込み">
			</label>
		</form>

		<div class="messages">
			<c:forEach items="${messages}" var="message">

				<div class="message">

					<div class="account-name">
						<span class="name"> <c:out value="${message.name}" />
						</span> <span class="account"> <c:out value="${message.account}" />
						</span>
					</div>

					<div class="title">
						<c:out value="${message.title}" />
					</div>

					<div class="category">
						<c:out value="${message.category}" />
					</div>

					<c:forEach  items="${message.splitedText}" var="m">
						<div class="text">
							<c:out value="${m}" />
						</div>
					</c:forEach>

					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
				</div>

				<c:if test="${loginUser.id == message.userId}" >
					<form action = "deleteMessage" method ="post" onSubmit="return check()">
						<input type="hidden" name="messageId" value="${message.id}">
						<input type="submit" value="削除">
					</form>
				</c:if>


				<div class="form-area">

					<form action="comment" method="post">
						コメント入力欄<br />
						<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
						<input type="hidden" name="messageId" value="${message.id}" />
						<input type="submit" value="投稿"><br />
					</form>

					<div class="comments">
						<c:forEach items="${comments}" var="comment">

							<c:if test="${message.id == comment.messageId }">

								<div class="comment">

									<div class="account-name">
										<span class="account"> <c:out value="${comment.account}" /></span>
										<span class="name"> <c:out value="${comment.name}" /></span>
									</div>

									<c:forEach  items="${comment.splitedText}" var="c">
										<div class="text">
											<c:out value="${c}" />
										</div>
									</c:forEach>

									<div class="date">
										<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
									</div>

								</div>
								<c:if test="${loginUser.id == comment.userId }">
									<form action = "deleteComment" method ="post" onSubmit="return check()">
										<input type="hidden" name="commentId" value="${comment.id}">
										<input  type="submit" value="削除">
									</form>
								</c:if>
							</c:if>
						</c:forEach>
					</div>
				</div>
			</c:forEach>
		</div>

		<div class="copyright">Copyright(c)usui_arisa</div>

	</div>
</body>
</html>