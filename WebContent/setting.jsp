<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー編集画面</title>

    </head>
    <body>

		<div class="main-contents">
			<a href="management">ユーザー管理</a>
		</div>

		<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <c:remove var="errorMessages" scope="session"/>


		<form action="setting" method="post">

			<label for="account">アカウント</label>
			<input name="account" value="${user.account}"/><br />

			<label for="password">パスワード</label>
			<input name="password" type="password" /> <br />

			<label for="confirmationPassword">確認用パスワード</label>
			<input name="confirmationPassword" type="password"/> <br />

			<label for="name">名前</label>
			<input name="name" value="${user.name }" /><br />

			<p>支社</p>
			<select name="branchId">
				<c:if test="${user.id != loginUser.id}">
					<c:forEach items="${branches}" var="branch">
						<option value="${branch.id}"<c:if test ="${branch.id == user.branchId}">selected</c:if>>${branch.name}</option>
					</c:forEach>
				</c:if>

				<c:if test="${user.id == loginUser.id}">
					<c:forEach items="${branches}" var="branch">
						<c:if test="${branch.id == user.branchId }"><option value="${branch.id}">${branch.name}</option></c:if>
					</c:forEach>
				</c:if>
			</select>



			<p>部署<p>
			<select name="departmentId">
				<c:if test="${user.id != loginUser.id}">
					<c:forEach items="${departments}" var="department">
						<option value="${department.id}"<c:if test = "${department.id == user.departmentId}">selected</c:if>>${department.name}</option>
					</c:forEach>
				</c:if>

				<c:if test="${user.id == loginUser.id}">
					<c:forEach items="${departments}" var="department">
						<c:if test="${department.id == user.departmentId }"><option value="${department.id}">${department.name}</option></c:if>
					</c:forEach>
				</c:if>
			</select><br>


			<input type="hidden" name="userId" value="${user.id}">
			<input type="submit" value="更新" /> <br />
		</form>

		<div class="copyright"> Copyright(c)usui_arisa</div>

    </body>
</html>