<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>新規投稿画面</title>
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

			<c:remove var="errorMessages" scope="session"/>

            <form action="message" method="post"><br />
            	<a href="./">ホーム</a>

                <label for="title">件名</label>
                <input name="title" id="title" value="${title}"/><br />

                <label for="category">カテゴリー</label>
                <input name="category" id="category" value="${category}" /> <br />

                <label for="text">投稿内容</label>
                <textarea name="text" cols="35" rows="5" id="text">${text}</textarea> <br />
                <input type="submit" value="登録" /> <br />
            </form>

			<c:remove var="title" scope="session"/>
			<c:remove var="category" scope="session"/>
			<c:remove var="text" scope="session"/>

            <div class="copyright">Copyright(c)usui_arisa</div>
        </div>
    </body>
</html>