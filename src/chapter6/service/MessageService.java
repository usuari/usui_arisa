package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String startDate,String endDate,String categoryWord) {

        final int LIMIT_NUM = 1000;

    	//引数としてデータを格納するための箱（変数）を用意
    	String start = "";
    	String end = "";
    	String category= "";
    	//開始時刻
    	if(startDate == null || startDate.isEmpty()) {
    		start ="2020-01-01 00:00:00";
    	} else {
    		start = (startDate +" 00:00:00");
    	}

    	//終了時刻
    	if(endDate == null || endDate.isEmpty()) {
    		Date today =new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
    		end = sdf.format(today);
    	}else {
    		end = (endDate + " 23:59:59");
    	}

    	//カテゴリー
    	if(!(categoryWord == null || categoryWord.isEmpty())){
    		category= "\"%"+categoryWord+"%\"";
    	}else {
    		category= null;
    	}


    	Connection connection = null;
        try {
            connection = getConnection();

            System.out.println(start);
        	System.out.println(end);
        	System.out.println(category);
        	System.out.println("");

            List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM, start, end, category);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}