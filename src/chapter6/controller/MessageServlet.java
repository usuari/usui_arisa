package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("message.jsp").forward(request, response);
		}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    		throws IOException, ServletException {

    	HttpSession session = request.getSession();

    	List<String> errorMessages = new ArrayList<String>();

    	String title = request.getParameter("title");
        String category = request.getParameter("category");
        String text = request.getParameter("text");

        if (!(isValid(text,title,category, errorMessages))){
        	session.setAttribute("errorMessages", errorMessages);
            session.setAttribute("title",title );
            session.setAttribute("category",category);
            session.setAttribute("text", text);
            response.sendRedirect("./message");
            return;
        }

        Message message = new Message();
        message.setTitle(title);
        message.setCategory(category);
        message.setText(text);

        User user = (User) session.getAttribute("loginUser");
        message.setUserId(user.getId());

        new MessageService().insert(message);
        response.sendRedirect("./");
    }

    private boolean isValid(String text, String title,String category,List<String> errorMessages) {

    	if (StringUtils.isBlank(title)){
    		errorMessages.add("件名を入力してください");
    	}

    	if (30<= title.length()){
    		errorMessages.add("件名を30文字以下で、入力してください");
    	}

    	if (StringUtils.isBlank(category)) {
    		errorMessages.add("カテゴリー名を入力してください");
    	}

    	if (10 < category.length()) {
    		errorMessages.add("カテゴリ－名は10文字以下で入力してください");
    	}

    	if (StringUtils.isBlank(text)) {
    		errorMessages.add("本文を入力してください");
    	}

    	if (1000 <= text.length()) {
    		errorMessages.add("1000文字以下で、本文を入力して下さい");
    	}

    	if (errorMessages.size() != 0) {
    		return false;
        }

        return true;
    }

}