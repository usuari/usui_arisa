package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.UserBranchDepartment;
import chapter6.service.UserService;

	@WebServlet(urlPatterns = { "/management" })
	public class ManagementServlet extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws IOException, ServletException {

			List<UserBranchDepartment> userList = new UserService().select();

			request.setAttribute("userList", userList);
			request.getRequestDispatcher("/management.jsp").forward(request, response);
		    }

	}
