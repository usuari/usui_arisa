package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Branch;
import chapter6.beans.Department;
import chapter6.beans.User;
import chapter6.service.BranchService;
import chapter6.service.DepartmentService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch>branches = new BranchService().select();
    	List<Department>departments = new DepartmentService().select();

    	request.setAttribute("branches",branches);
    	request.setAttribute("departments",departments);

        request.getRequestDispatcher("signup.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<String> errorMessages = new ArrayList<String>();

        User user = getUser(request);

        if (!isValid(user, errorMessages)) {
        	request.setAttribute("errorMessages", errorMessages);
        	request.setAttribute("branches",new BranchService().select());
        	request.setAttribute("departments",new DepartmentService().select());
        	request.setAttribute("user", user);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }

        new UserService().insert(user);
        response.sendRedirect("./management");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

    	User user = new User();

        user.setAccount(request.getParameter("account"));
        user.setName(request.getParameter("name"));
        user.setPassword(request.getParameter("password"));
        user.setConfirmationPassword(request.getParameter("confirmationPassword"));
      	user.setBranchId(Integer.valueOf(request.getParameter("branchId")));
        user.setDepartmentId(Integer.valueOf(request.getParameter("departmentId")));
        return user;
    }

    private boolean isValid(User user, List<String> errorMessages) {

    	String account = user.getAccount();
        String name = user.getName();
        String password = user.getPassword();
        String confirmationPassword = user.getConfirmationPassword();
        String branchId =String.valueOf(user.getBranchId());
        String departmentId =String.valueOf(user.getDepartmentId());

        User userCheck = new UserService().select(account);

        if (StringUtils.isEmpty(name)) {
        	errorMessages.add("名前を入力してください");
        }else  if (!StringUtils.isEmpty(name) && (10 < name.length())) {
            errorMessages.add("名前は10文字以下で入力してください");
        }

        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        }else if (6 > account.length() || 20 < account.length()) {
            errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
        }else if (!(account.matches("^[A-Za-z0-9]+$"))) {
        	errorMessages.add("アカウント名は半角英数字で入力してください");
        }

        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        }else if (6 > password.length() || 20 < password.length()) {
			errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
		}

		if (StringUtils.isEmpty(confirmationPassword)) {
			errorMessages.add("確認用パスワードを入力してください");
		}else if (6 > confirmationPassword.length() || 20 < confirmationPassword.length()) {
			errorMessages.add("確認用パスワードは6文字以上20文字以下で入力してください");
		}

		if(!(password.equals(confirmationPassword)) ){
            errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
        }

        if (StringUtils.isEmpty(branchId)) {
            errorMessages.add("支社を選択してください");
        }

        if (StringUtils.isEmpty(departmentId)) {
            errorMessages.add("部署を選択してください");
        }

        //本社　人事部と情報管理部
        if(Integer.valueOf(branchId)==1 && (Integer.valueOf(departmentId) ==3)){
         	errorMessages.add("支社と部署が異なります");
        }
        if(Integer.valueOf(branchId)==1 && (Integer.valueOf(departmentId) ==4)){
         	errorMessages.add("支社と部署が異なります");
        }
         //A社　営業部と技術部
        if(Integer.valueOf(branchId)==2 && (Integer.valueOf(departmentId) ==1)){
         	errorMessages.add("支社と部署が異なります");
        }
        if(Integer.valueOf(branchId)==2 && (Integer.valueOf(departmentId) ==2)){
         	errorMessages.add("支社と部署が異なります");
        }
         //B支社　営業部と技術部
        if(Integer.valueOf(branchId)==3 && (Integer.valueOf(departmentId) ==1)){
         	errorMessages.add("支社と部署が異なります");
        }
        if(Integer.valueOf(branchId)==3 && (Integer.valueOf(departmentId) ==2)){
         	errorMessages.add("支社と部署が異なります");
        }
         //C支社　営業部と技術部
        if(Integer.valueOf(branchId)==4 && (Integer.valueOf(departmentId) ==1)){
         	errorMessages.add("支社と部署が異なります");
        }
        if(Integer.valueOf(branchId)==4 && (Integer.valueOf(departmentId) ==2)){
         	errorMessages.add("支社と部署が異なります");
        }
         //ユーザーの重複
        if(userCheck != null){
        	errorMessages.add("アカウントが重複しています");
        }

        if (errorMessages.size() == 0) {
        	return true;
		} else {
			return false;
		}

    }

}