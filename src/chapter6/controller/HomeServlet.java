package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/" })
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String category =request.getParameter("category");

		List<UserMessage> messages = new MessageService().select(start, end, category);
		List<UserComment> comments = new CommentService().select();

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("start", start);
		request.setAttribute("end", end);
		request.setAttribute("category", category);

		request.getRequestDispatcher("/home.jsp").forward(request, response);

    }
}