package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.UserBranchDepartment;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {


		List<UserBranchDepartment> userList = new UserService().select();

		request.setAttribute("userList", userList);
		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		int userId = Integer.valueOf(request.getParameter("userId"));
		int isStopped = Integer.valueOf(request.getParameter("isStopped"));

		new UserService().stop(userId,isStopped);

		response.sendRedirect("./management");
	}

}
