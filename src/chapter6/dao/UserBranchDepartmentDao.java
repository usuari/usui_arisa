package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserBranchDepartment;
import chapter6.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {
	public List<UserBranchDepartment> select(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT");
			sql.append(" users.id AS id,");
			sql.append(" users.name AS name,");
			sql.append(" users.account AS account,");
			sql.append(" users.is_stopped AS is_stopped,");
			sql.append(" branches.id AS branch_id,");
			sql.append(" branches.name AS branch_name,");
			sql.append(" departments.id AS department_id,");
			sql.append(" departments.name AS department_name,");
			sql.append(" users.created_date AS created_date,");
			sql.append(" users.updated_date AS updated_date");
			sql.append(" FROM users");
			sql.append(" INNER JOIN branches ON users.branch_id = branches.id");
			sql.append(" INNER JOIN departments ON users.department_id = departments.id");
			sql.append(" ORDER BY created_date DESC;");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment >list = toUserBranchDepartment(rs);

			return list;
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}

	}

	private List<UserBranchDepartment> toUserBranchDepartment(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> lists = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				UserBranchDepartment list = new UserBranchDepartment();
				list.setId(rs.getInt("id"));
				list.setName(rs.getString("name"));
				list.setAccount(rs.getString("account"));
				list.setIsStopped(rs.getInt("is_stopped"));
				list.setBranchId(rs.getInt("branch_id"));
				list.setBranchName(rs.getString("branch_name"));
				list.setDepartmentId(rs.getInt("department_id"));
				list.setDepartmentName(rs.getString("department_name"));
				list.setCreatedDate(rs.getDate("created_date"));
				list.setUpdatedDate(rs.getDate("updated_date"));

				lists.add(list);
			}

			return lists;
			} finally {
				close(rs);
			}
	}

}
