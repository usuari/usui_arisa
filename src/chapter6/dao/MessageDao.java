package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

  public void insert(Connection connection, Message message) {

    PreparedStatement ps = null;
    try {
    	StringBuilder sql = new StringBuilder();
    	sql.append("INSERT INTO messages ( ");
    	sql.append("    title, ");
    	sql.append("    text, ");
    	sql.append("    category, ");
    	sql.append("    user_id, ");
    	sql.append("    created_date, ");
    	sql.append("    updated_date ");
    	sql.append(") VALUES ( ");
    	sql.append("    ?, ");                   //title
    	sql.append("    ?, ");  			     //text
    	sql.append("    ?, ");                   //category
    	sql.append("    ?, ");        		     //user_id
    	sql.append("    CURRENT_TIMESTAMP, ");   // created_date
    	sql.append("    CURRENT_TIMESTAMP ");    // updated_date
    	sql.append(");");

    	ps = connection.prepareStatement(sql.toString());

    	ps.setString(1, message.getTitle());
    	ps.setString(2, message.getText());
    	ps.setString(3, message.getCategory());
    	ps.setInt(4, message.getUserId());

    	ps.executeUpdate();

    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
  }

  public void delete(Connection connection , int messageId) {

	  PreparedStatement ps = null;
	  try {
		  StringBuilder sql = new StringBuilder();
		  //DELETE FROM テーブル名 [WHERE 条件式];
		  sql.append("DELETE FROM messages WHERE id = ? ");

		  ps = connection.prepareStatement(sql.toString());

		  ps.setInt(1, messageId);

		  ps.executeUpdate();

	  }catch (SQLException e ) {
		  throw new SQLRuntimeException(e);
	  }finally {
		  close(ps);
	  }
  }

}