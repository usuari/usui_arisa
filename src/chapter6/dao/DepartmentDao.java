package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Department;
import chapter6.exception.SQLRuntimeException;

public class DepartmentDao {
	public List<Department> select(Connection connection ,int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT");
			sql.append(" id,");
			sql.append(" name,");
			sql.append(" created_date,");
			sql.append(" updated_date");
			sql.append(" FROM");
			sql.append(" departments;");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Department>department = toDepartment(rs);

			return department;
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	}

	private List<Department> toDepartment(ResultSet rs) throws SQLException {

		List<Department> departments = new ArrayList<Department>();
		try {
			while (rs.next()) {
				Department department = new Department();

				department.setId(rs.getInt("id"));
				department.setName(rs.getString("name"));
				department.setCreatedDate(rs.getDate("created_date"));
				department.setUpdatedDate(rs.getDate("updated_date"));

				departments.add(department);
				}

			return departments;
			} finally {
				close(rs);
			}
	}

}
