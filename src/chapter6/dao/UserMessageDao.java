package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.UserMessage;
import chapter6.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> select(Connection connection, int num, String startDate, String endDate,String category) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    messages.id AS id, ");
            sql.append("    users.name AS name, ");
            sql.append("    users.account AS account, ");
            sql.append("    messages.title AS title, ");
            sql.append("    messages.category AS category, ");
            sql.append("    messages.text AS text, ");
            sql.append("    messages.user_id AS user_id, ");
            sql.append("    messages.created_date AS created_date, ");
            sql.append("    messages.updated_date AS updated_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_date BETWEEN " + "?" + " AND "+ "?");
            if(!(category == null || category.isEmpty())) {
            	sql.append(" AND messages.category LIKE "+ "?");
            }
            sql.append(" ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, startDate);
        	ps.setString(2, endDate);
        	if(!(StringUtils.isEmpty(category))) {
        	ps.setString(3, category);
        	}

            System.out.println(ps.toString());

            ResultSet rs = ps.executeQuery();

            List<UserMessage> messages = toUserMessages(rs);

            return messages;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                UserMessage message = new UserMessage();
                message.setId(rs.getInt("id"));
                message.setTitle(rs.getString("title"));
                message.setCategory(rs.getString("category"));
                message.setText(rs.getString("text"));
                message.setUserId(rs.getInt("user_id"));
                message.setCreatedDate(rs.getTimestamp("created_date"));
                message.setUpdatedDate(rs.getTimestamp("updated_date"));
                message.setName(rs.getString("name"));
                message.setAccount(rs.getString("account"));
                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }

}