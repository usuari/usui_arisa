package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {

    private int id;
    private String title;
    private String category;
    private String text;
    private int userId;
    private Date createdDate;
    private Date updatedDate;
    private String name;
    private String account;

    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    public String getTitle() {
    	return title;
    }
    public void setTitle(String title) {
    	this.title = title;
    }

    public String getCategory() {
    	return category;
    }
    public void setCategory(String category) {
    	this.category = category;
    }

    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }

 // 追加したメソッド
    public String[] getSplitedText() {
        return text.split("\n");
    }

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}

}
